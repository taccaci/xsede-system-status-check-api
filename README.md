#XSEDE System Status Check API

This is a simple status check API which returns basis system info and
queue info. It is sufficient for use in Pingdom type checks of system
load and availability.

## Building

Update the `/slim/.env.php` file with a valid set of XUP credentials to use when authenticating to the XSEDE APIs. You can rename the `/slim/.env.php.example` file and use that as a starting point. Once set, build the Docker image manually or via the included Fig file.

```bash  

$ docker build -t taccaci/xsede-system-status-check-api .  

```  

## Running  

This API makes heavy use of Redis for caching. Use the included Fig
file to orchestrate the container startup.

```bash  

$ docker-compose up -d  

```  

The API will be available at `http://docker.example.com:9080`.
