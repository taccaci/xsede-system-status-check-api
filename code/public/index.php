<?php

/**
 * XSEDE System Load and Availability Mashup API
 *
 * @package  Agave
 * @author   Rion Dooley <dooley@tacc.utexas.edu>
 */

require '../vendor/autoload.php';

use Karnak\SystemNotFoundException;
use Karnak\KarnakException;

class ResourceNotFoundException extends \Exception {}
class ServiceUnavailableFoundException extends \Exception {}
class PermissionException extends \Exception {}
class AuthenticationException extends \Exception {}

require '../app/bootstrap.php';
require '../app/cache.php';
require '../app/config.php';
require '../app/routes.php';

$app->run();
