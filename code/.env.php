<?php

function getEnvOrDefault($varName, $default='') {
    if (empty($varName)) {
        return $default;
    } else {
        $envVarName = strtoupper($varName);
        $envVarName = str_replace('.', '_', $varName);
        $val = getenv($envVarName);
        return (empty($val) ? $default : $val);
    }
}

$xsede_api_base = getEnvOrDefault('XSEDE_API_BASE', 'https://api.xsede.org');
$xup_username = getEnvOrDefault('XSEDE_SERVICE_USERNAME', 'username');
$xup_password = getEnvOrDefault('XSEDE_SERVICE_PASSWORD', 'password');
$service_hostname = getEnvOrDefault('HOSTNAME', 'docker.example.com');
$base_url = 'http://'.$service_hostname;

?>
