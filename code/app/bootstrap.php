<?php


function formatErrorMessage($message)
{
  global $app;

  echo json_encode([
    "status" => "error",
    "code" => $app->response->getStatus(),
    "message" => $message
    ]);
}

$app = new \Slim\Slim(array(
    'mode' => 'development'
));

$app->log->setLevel(\Slim\Log::DEBUG);
