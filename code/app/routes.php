<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}

// return HTTP 200 for HTTP OPTIONS requests
$app->map('/:x+', function($x) {
    http_response_code(200);
})->via('OPTIONS');

$app->notFound(function() use ($app) {
    $app->response()->status(404);
    formatErrorMessage("No matching resource found");
});

$app->error(function (\Exception $e) use ($app) {
  $app->response()->status(500);
  formatErrorMessage($e->getMessage());
});

/**
 * Returns media listing
 *
 * @return array of @SystemSummary objects
 */
$app->get('/', function () use ($app, $redis) {
  try {
    $swagger = json_decode(file_get_contents(DOCUMENT_ROOT.'/index.json'));
    $parsedUrl = parse_url(BASE_URL);
    $swagger->host = $parsedUrl[host] . ($parsedUrl['port'] !== 80 ? ":".$parsedUrl['port'] : '');
    $swagger->basePath = $parsedUrl[path];
    echo json_encode($swagger);
  } catch (Exception $e) {
    $app->response()->status(500);
    formatErrorMessage($e->getMessage());
  }
});

/**
 * Returns array of SystemSummary objects
 *
 * @return array of @SystemSummary objects
 */
$app->get('/systems', function () use ($app, $redis) {
  try {
    $xsede = ClientUtil::getXsedeClient(API_USERNAME, API_PASSWORD);
    $systems = $xsede->getSystems();

    $response = [];
    foreach($systems->result as $system) {
        $response[] = [
          'hostname' => $system->hostname,
          'status' => $system->status,
          'resource_name' => $system->resource_name,
          'peak_performance' => $system->peak_performance,
          'memory' => $system->hostname,
          'nickname' => $system->nickname,
          'resource_type' => $system->resource_type,
          'load' => $system->load,
          'jobs' => $system->jobs,
          '_links' => [
            'self' => [
              'href' => BASE_URL . "/systems/" . $system->hostname . "/status"
            ]
          ]
        ];
    }

    echo json_encode($response, TRUE);
  } catch (AuthenticationException $e) {
    $app->response()->status(401);
    formatErrorMessage($e->getMessage());
  } catch (\XSEDE\XSEDEException $e) {
    $app->response()->status(502);
    formatErrorMessage($e->getMessage());
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
  } catch (Exception $e) {
    $app->response()->status(500);
    formatErrorMessage($e->getMessage());
  }
});


/**
 * Returns details of named system
 *
 * @var string $host value of the system
 * @return @SystemDetail object
 */
$app->get('/systems/:systemId', function ($systemId) use ($app, $redis) {
  try
  {
    $xsede = ClientUtil::getXsedeClient(API_USERNAME, API_PASSWORD);
    $system = $xsede->getDetail($systemId);
    if (empty($system)) {
      throw new ResourceNotFoundException("No system found with host id $systemId");
    } else {
      $system->result[0]->_links = [
        'self' => [
          'href' => BASE_URL . "/systems/$systemId"
        ]
      ];
      echo json_encode($system->result[0]);
    }
  } catch (AuthenticationException $e) {
    $app->response()->status(401);
    formatErrorMessage($e->getMessage());
  } catch (XSEDE\XSEDEException $e) {
    $app->response()->status(502);
    formatErrorMessage($e->getMessage());
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
    formatErrorMessage($e->getMessage());
  } catch (Exception $e) {
    $app->response()->status(500);
    formatErrorMessage($e->getMessage());
  }
});

/**
 * Returns status of the named system.
 *
 * @var string $host value of the system
 * @return @SystemLoad status objects
 */
$app->get('/systems/:systemId/status', function ($systemId) use ($app, $redis) {
  try
  {
    $system = ClientUtil::getXsedeClient(API_USERNAME, API_PASSWORD)->getDetail($systemId);

    if (empty($system) || empty($system->result)) {
      $status = -1;
    } else if (strtolower($system->result[0]->status) == 'up') {
      $status = 1;
    } else {
      $status = 0;
    }

    echo json_encode([
      'status' => $status,
      '_links' => [
        'self' => [
          'href' => (BASE_URL . "/systems/$systemId/status")
        ],
        'system' => [
          'href' => (BASE_URL . "/systems/$systemId")
        ]
      ]]);
  } catch (AuthenticationException $e) {
    $app->response()->status(401);
    formatErrorMessage($e->getMessage());
  } catch (\XSEDE\XSEDEException $e) {
    $app->response()->status(502);
    formatErrorMessage($e->getMessage());
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
  } catch (Exception $e) {
    $app->response()->status(500);
    formatErrorMessage($e->getMessage());
  }
});

/**
 * Returns all queues for a given system with current loads.
 *
 * @var string $host value of the system
 * @return array of @SystemQueue objects
 */
$app->get('/systems/:systemId/queues', function ($systemId) use ($app, $redis) {
  try
  {
    $karnak = new \Karnak\KarnakClient();

    $queues = $karnak->getSystemQueueStatuses($systemId);

    if (!isset($queues->Queue)) {
      echo "[]";
    } else {
      $response  = [];
      foreach($queues->Queue as $queue) {
        $response[] = [
          'name'=>$queue->Name,
          'load' => [
            'running' => $queue->JobSummary->NumRunningJobs,
            'waiting' => $queue->JobSummary->NumWaitingJobs
          ],
          '_links' => [
            'self' => [
              'href' => BASE_URL . "/systems/$systemId/queues/" . $queue->Name
            ],
            'system' => [
              'href' => BASE_URL . "/systems/$systemId"
            ]
          ]];
      }
      echo json_encode($response);
    }
  } catch (\Karnak\KarnakException $e) {
    $app->response()->status(502);
    formatErrorMessage($e->getMessage());
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
  } catch (Exception $e) {
    $app->response()->status(500);
    formatErrorMessage($e->getMessage());
  }
});

/**
 * Returns the named queues on the named system with current loads.
 *
 * @var string $host value of the system
 * @var string $queue unique name of the queue on the given system
 * @return @SystemQueue object
 */
$app->get('/systems/:systemId/queues/:queueId', function ($systemId, $queueId) use ($app, $redis)
{
  try
  {
    $karnak = new \Karnak\KarnakClient();

    $queue = $karnak->getSystemQueueStatus($systemId, $queueId);

    if (empty($queue)) {
      echo "[]";
    } else {
      echo json_encode([
        'name'=>$queue->Name,
        'load' => [
          'running' => $queue->JobSummary->NumRunningJobs,
          'waiting' => $queue->JobSummary->NumWaitingJobs
        ],
        '_links' => [
          'self' => [
            'href' => BASE_URL . "/systems/$systemId/queues/$queueId"
          ],
          'system' => [
            'href' => BASE_URL . "/systems/$systemId"
          ]
        ]]);
    }
  } catch (\Karnak\KarnakException $e) {
    $app->response()->status(502);
    formatErrorMessage($e->getMessage());
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
  } catch (Exception $e) {
    $app->response()->status(500);
    formatErrorMessage($e->getMessage());
  }
});
