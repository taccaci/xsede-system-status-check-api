<?php

date_default_timezone_set('America/Chicago');

/******************************************************
 *
 * ENVIRONMENT
 *
 * Sensitive data is sourced from the .env file in the
 * project root.
 *
 ******************************************************/

define('DOCUMENT_ROOT', getenv('DOCUMENT_ROOT'));

if (!file_exists(DOCUMENT_ROOT.'/../.env.php')) {
  $app->response()->status(500);
  formatErrorMessage("Environment configuration file missing. Cannot contact XSEDE API.");
}

include DOCUMENT_ROOT.'/../.env.php';

if (empty($baseUrl)) {
  $baseUrl = getenv('BASE_URL');
}
if (empty($baseUrl)) {
  $baseUrl = 'http://docker.example.com:9080';
}

define('API_BASE_URL', $xsede_api_base);
define('API_USERNAME', $xup_username);
define('API_PASSWORD', $xup_password);
define('BASE_URL', $base_url);
