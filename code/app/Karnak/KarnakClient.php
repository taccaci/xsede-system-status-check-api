<?php namespace Karnak;

use Karnak\KarnakException;
use Karnak\SystemNotFoundException as SystemNotFoundException;

/**
 * PHP Client for the Karnak Prediction Service
 */
class KarnakClient
{
  private $baseUrl = 'http://karnak.xsede.org/karnak/';

  public function __construct() {}

  /**
   * Returns a valid auth token for the XSEDE API. If no token is
   * cached, it retrieves one and caches it for future use in Redis.
   */
  public function getAllStatuses()
  {
    global $redis, $app;

    $key = "karnak_status";
    try
    {
      $statuses = $redis->get($key);

      if (empty($statuses))
      {
        $app->log->debug("Karnak status cache expired. Retrieving fresh statuses");

        $response = $this->doGet($this->baseUrl."system/status.xml");

        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $statuses = json_decode($json);

        if (isset($statuses)) {
          $redis->set($key, $statuses);
          $redis->expireat($key, strtotime('+5 minutes'));
          $app->log->debug("Statuses retrieved and cached for 5 minutes.");
        } else {
          throw new SystemNotFoundException("No statuses returned from Karnak.");
        }

        return $statuses;
      }
      else
      {
        return $statuses;
      }
    }
    catch (SystemNotFoundException $e) {
      throw $e;
    }
    catch (\Exception $e) {
      throw new KarnakException($e->getMessage());
    }
  }

  /**
   * Returns an array of system queues with load information.
   *
   * @param array of @SystemQueue objects
   */
  public function getSystemQueueStatuses($host)
  {
    global $redis, $app;

    $key = "karnak_".$host."_queues";
    try
    {
      //$queues = $redis->get($key);

      if (empty($queues))
      {
        $app->log->debug("Karnak status cache expired. Retrieving fresh statuses");

        $response = $this->doGet($this->baseUrl."system/$host/status.xml");

        $xml = simplexml_load_string($response);
        // $app->log->debug($xml);
        $json = json_encode($xml);
        // $app->log->debug($json);
        $queues = json_decode($json);
        // $app->log->debug($queues);

        if (isset($queues)) {
          $app->log->debug("Found queues for $host");
          $redis->set($key, $json);
          $redis->expireat($key, strtotime('+1 day'));
          $app->log->debug("$host queues retrieved and cached for 1 day.");
        } else {
          throw new SystemNotFoundException("No queues returned for $host from Karnak.");
        }

        return $queues;
      }
      else
      {
        return json_decode($queues);
      }
    }
    catch (SystemNotFoundException $e) {
      throw $e;
    }
    catch (\Exception $e) {
      throw new KarnakException($e->getMessage());
    }
  }

  /**
   * Returns an array of system queues with load information.
   *
   * @param array of @SystemQueue objects
   */
  public function getSystemQueueStatus($host, $queueName)
  {
    global $app;

    try
    {
      $queues = $this->getSystemQueueStatuses($host);

      if (empty($queueName)) {
        throw new SystemNotFoundException("No queue name supplied.");
      } else {
        $app->log->debug($queues);
        foreach($queues->Queue as $queue) {
          if ($queue->Name == $queueName) {
            $app->log->debug("Found queue $queueName on host $host");
            return $queue;
          }
        }
      }
      $app->log->error("No queue with name $queueName found on host $host");
      throw new SystemNotFoundException("No queue found matching name $queueName");
    }
    catch (SystemNotFoundException $e) {
      throw $e;
    }
    catch (\Exception $e) {
      throw new KarnakException($e->getMessage());
    }
  }

  function doGet($url)
  {
    global $app;

  	$app->log->debug("Calling GET on $url");

  	$curl = curl_init();
  	curl_setopt($curl, CURLOPT_HEADER, false);
  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  	curl_setopt($curl, CURLOPT_URL, $url);

  	$response = curl_exec($curl);
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);

    if ($httpCode === 200) {
      // $app->log->debug("Successfully retrieved response from karnak: $response");
      return $response;
    } else if ($httpCode === 401 || $httpCode === 403) {
      $app->log->error("[$httpCode] Karnak requests require authentication.");
      return null;
    } else {
      $app->log->error("Failed to query Karnak service: $response");
      return null;
    }
  }
  /**
 * Perform an http post using basic auth on the endpoint
 */
  function doPost($url, $uname, $pass, $form_vars=array())
  {
    global $app;

  	$post_data = serialize_form_data($form_vars);

    $app->log->debug("Calling POST on $url with $post_data");

  	$curl = curl_init();
  	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
  	curl_setopt($curl, CURLOPT_USERPWD, $uname.":".$pass);
  	curl_setopt($curl, CURLOPT_SSLVERSION,3);
  	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
  	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
  	curl_setopt($curl, CURLOPT_HEADER, false);
  	curl_setopt($curl, CURLOPT_POST, true);
  	curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data );
  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  	curl_setopt($curl, CURLOPT_URL, $url);

  	$response = curl_exec($curl);
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);

    if ($httpCode === 200) {
      $app->log->debug("Successfully retrieved token: $response");
      return $response;
    } else if ($httpCode === 401 || $httpCode === 403) {
      $app->log->error("[$httpCode] Failed to authenticate to the upstream server.");
      return null;
    } else {
      $app->log->error("Authentication failed: $response");
      return null;
    }
  }

}
