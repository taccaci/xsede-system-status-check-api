<?php namespace Karnak;

class KarnakException extends \Exception {}

class SystemNotFoundException extends KarnakException {}
