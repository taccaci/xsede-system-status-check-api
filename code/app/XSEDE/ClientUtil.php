<?php

use \Predis\PredisException;

class ClientUtil
{
  /**
   * Authenticates and pulls a valid api token for use interacting
   * with the rest of the API.
   *
   * @param string $username XUP username
   * @param string $password XUP password
   * @return @Token Valid authentication token
   * @throws @AuthenticationException if login fails when pulling a token
   * @throws @XSEDEException on failure to communicate with XSEDE
   */
  public static function getApiToken($username, $password)
  {
    global $app, $redis;

    $key = "xsede_auth_token";

    $token = $redis->get($key);
    try
    {
      if (empty($token))
      {
        $app->log->debug("Token cache expired. Retrieving fresh token");
        $basicClient = new APIClient(API_BASE_URL, 'Authorization', "Basic " . base64_encode($username.":".$password));
        $xsede = new DefaultApi($basicClient);
        $tokenObject = $xsede->getToken();

        if (isset($tokenObject->result[0]->token)) {
          $redis->set($key, $tokenObject->result[0]->token);
          $expires = strtotime($tokenObject->result[0]->expires);
          $redis->expireat($key, $expires);
          $app->log->debug("Token retrieved and cached until ". $tokenObject->result[0]->expires);
        } else {
          throw new XSEDEException("No token returned from upstream authentication server.");
        }

        return $tokenObject->result[0]->token;
      }
      else
      {
        return $token;
      }
    } catch (\Predis\PredisException $e) {
      $app->log->error($e->getMessage());
      throw new XSEDEException("Failed to cache info ". $e->getMessage());
    } catch (Exception $e) {
      $app->log->error($e->getMessage());
      throw new XSEDEException("Unable to retrieve token ". $e->getMessage());
    }
  }

  /**
   * Returns a shiny new XSEDE API client. Auth is handled magically.
   *
   * @param string $username XUP username
   * @param string $password XUP password
   * @return @DefaultApi XSEDE api client
   * @throws @AuthenticationException if login fails when pulling a token
   * @throws @XSEDEException on failure to communicate with XSEDE
   */
  public static function getXsedeClient($username, $password)
  {
    try
    {
      $xsede = new DefaultApi(
                new APIClient(API_BASE_URL, 'Authorization',
                  "Basic " . base64_encode($username .":". self::getApiToken($username, $password))));
      return $xsede;
    } catch (AuthenticationException $e) {
      throw $e;
    } catch (XSEDEException $e) {
      throw $e;
    } catch (Exception $e) {
      throw new XSEDEException($e->getMessage());
    }
  }
}
