<?php namespace Agave;

use Agave\QueueLoad as QueueLoad;

class SystemQueue
{
  /**
   * The name of the queue.
   *
   * @var int
   */
  private $name;


  /**
   * Average wait time for current jobs in this queue.
   *
   * @var int
   */
  private $avgWait;

  /**
   * Current load on this queue
   *
   * @var QueueLoad
   */
  private $load;

  public __construct() {}

  /**
   * Get the value of The name of the queue.
   *
   * @return int
   */
  public function getName()
  {
      return $this->name;
  }

  /**
   * Set the value of The name of the queue.
   *
   * @param int name
   *
   * @return self
   */
  public function setName($name)
  {
      $this->name = $name;

      return $this;
  }

  /**
   * Get the value of Average wait time for current jobs in this queue.
   *
   * @return int
   */
  public function getAvgWait()
  {
      return $this->avgWait;
  }

  /**
   * Set the value of Average wait time for current jobs in this queue.
   *
   * @param int avgWait
   *
   * @return self
   */
  public function setAvgWait($avgWait)
  {
      $this->avgWait = $avgWait;

      return $this;
  }

  /**
   * Get the value of Current load on this queue
   *
   * @return QueueLoad
   */
  public function getLoad()
  {
      return $this->load;
  }

  /**
   * Set the value of Current load on this queue
   *
   * @param QueueLoad load
   *
   * @return self
   */
  public function setLoad(QueueLoad $load)
  {
      $this->load = $load;

      return $this;
  }

}
