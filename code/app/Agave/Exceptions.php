<?php namespace Agave

class ResourceNotFoundException extends \Exception {}
class ServiceUnavailableFoundException extends \Exception {}
class PermissionException extends \Exception {}
class AuthenticationException extends \Exception {}
