<?php namespace Agave;

/**
 * Model to contain system load information. Basically just queue information
 */
class QueueLoad
{
  /**
   * Currently running jobs
   *
   * @var int
   */
  private $running;

  /**
   * Currently queued jobs
   *
   * @var int
   */
  private $queued;

  /**
   * Currently undefined jobs
   *
   * @var int
   */
  private $other;

  public __construct($running=0, $queued=0, $other=0)
  {
    $this->running = $running;
    $this->queued = $queued;
    $this->other = $other;
  }

  /**
   * Get the value of Currently running jobs
   *
   * @return int
   */
  public function getRunning()
  {
      return $this->running;
  }

  /**
   * Set the value of Currently running jobs
   *
   * @param int running
   *
   * @return self
   */
  public function setRunning($running)
  {
      $this->running = $running;

      return $this;
  }

  /**
   * Get the value of Currently queued jobs
   *
   * @return int
   */
  public function getQueued()
  {
      return $this->queued;
  }

  /**
   * Set the value of Currently queued jobs
   *
   * @param int queued
   *
   * @return self
   */
  public function setQueued($queued)
  {
      $this->queued = $queued;

      return $this;
  }

  /**
   * Get the value of Currently undefined jobs
   *
   * @return int
   */
  public function getOther()
  {
      return $this->other;
  }

  /**
   * Set the value of Currently undefined jobs
   *
   * @param int other
   *
   * @return self
   */
  public function setOther($other)
  {
      $this->other = $other;

      return $this;
  }

}
