<?php namespace Agave;


/**
 * Represents an auth token from the xsede api.
 */
class AuthToken
{
  /**
   * Returns a valid auth token for the XSEDE API. If no token is
   * cached, it retrieves one and caches it for future use in Redis.
   */
  public static function getInstance() {
    global $redis;

    $token = $redis->get("api_token");

    if (empty($token))
    {
      $app->log->debug("Token cache expired. Retrieving fresh token");

      $token = getNewToken();

      if (isset($token->token)) {
        $redis->set("api_token", $token->token);
        $redis->expireat("api_token", strtotime($token->expires));
        $app->log->debug("Token retrieved and cached for 2 weeks.");
      } else {
        $app->halt(500, "No token returned from upstream authentication server.");
      }

      return $token->token;
    }
    else
    {
      return $token;
    }
  }

  /**
   * Retrieves a fresh token from the XSEDE Auth API.
   */
  public static function getNewToken()
  {
    global $app;

    $response = self::doPost(AUTH_API, XSEDE_USERNAME, XSED_PASSWORD);

    if ($response)
    {
      $json = json_decode($response);
      if (!empty($json))
      {
        if (isset($json->result)) {
          return $json->result[0];
        } else {
          $app->halt(500, "Failed to obtain authentication token for the XSEDE API.");
        }
      }
      else
      {
        $app->halt(500, "Unrecognized response from the upstream authentication server.");
      }
  }

  /**
 * Perform an http post using basic auth on the endpoint
 */
  function doPost($url, $uname, $pass, $form_vars=array())
  {
    global $app;

  	$post_data = serialize_form_data($form_vars);

    $app->log("Calling POST on $url with $post_data");

  	$curl = curl_init();
  	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
  	curl_setopt($curl, CURLOPT_USERPWD, $uname.":".$pass);
  	curl_setopt($curl, CURLOPT_SSLVERSION,3);
  	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
  	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
  	curl_setopt($curl, CURLOPT_HEADER, false);
  	curl_setopt($curl, CURLOPT_POST, true);
  	curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data );
  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  	curl_setopt($curl, CURLOPT_URL, $url);

  	$response = curl_exec($curl);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($curl);

    if ($httpCode === 200) {
      $app->log->debug("Successfully retrieved token: $response");
      return $response;
    } else if ($httpCode === 401 || $httpCode === 403) {
      $app->log->error("[$httpCode] Failed to authenticate to the upstream server.");
      return null;
    } else {
      $app->log->error("Authentication failed: $response");
      return null;
    }
  }

}
