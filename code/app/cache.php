<?php

/******************************************************
 *
 * CACHE
 *
 * We use a Redis cache to keep this API snappy.
 * Initialize it here.
 *
 ******************************************************/

use Predis\Command\CommandInterface;
use Predis\Connection\StreamConnection;

// Initialize the Redis server
$redis = new Predis\Client([
    'scheme' => 'tcp',
    'host'   => 'redis',
    'port'   => 6379,
]);
